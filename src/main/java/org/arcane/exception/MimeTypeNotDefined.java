package org.arcane.exception;

import org.springframework.http.HttpStatus;

public class MimeTypeNotDefined extends GenericNyazoException{
    public MimeTypeNotDefined(String msg) {
        super(HttpStatus.UNSUPPORTED_MEDIA_TYPE, msg);
    }

    public MimeTypeNotDefined() {
        super(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "mime type not defined");
    }
}
