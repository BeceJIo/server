package org.arcane.exception;

import org.springframework.http.HttpStatus;

public class NotFoundFileException extends GenericNyazoException{

    public NotFoundFileException(String msg) {
        super(HttpStatus.NOT_FOUND, msg);
    }

    public NotFoundFileException() {
        super(HttpStatus.NOT_FOUND, "Not found");
    }
}
