package org.arcane.exception;

import org.springframework.http.HttpStatus;

public class GenericNyazoException extends RuntimeException {

    private final HttpStatus status;
    private final String msg;

    public GenericNyazoException(HttpStatus status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public GenericNyazoException(String msg) {
        super(msg);
        status = HttpStatus.BAD_REQUEST;
        this.msg = msg;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
