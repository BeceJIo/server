package org.arcane.exception;

import org.springframework.http.HttpStatus;

public class FileNotCreatedException extends GenericNyazoException{

    public FileNotCreatedException(String msg) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, msg);
    }
}
