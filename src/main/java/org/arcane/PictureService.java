package org.arcane;

import org.arcane.auth.Auth;
import org.arcane.exception.FileNotCreatedException;
import org.arcane.storage.Storage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class PictureService {

    private static final Logger LOG = Logger.getLogger(PictureService.class);

    private final Storage storage;

    private final Auth auth;

    private final String URL_SERVER;


    public PictureService(Storage storage, Auth auth, @Value("${nyazo.server.url}") Optional<String> urlServer) {
        this.storage = storage;
        this.auth = auth;
        URL_SERVER = urlServer.orElseGet(() -> {
                LOG.errorv("Server domain name not defined");
                Runtime.getRuntime().halt(1);
                return "";
            }
        );
        LOG.infov("Server uses the address {0}", URL_SERVER);
    }


    public void deletePic(String fileName) {
        storage.delete(fileName);
    }

    public void deleteBeforeDate(LocalDateTime dateTime) {
        storage.gc(dateTime);
    }

    public InputStream getPic(String fileName) {
        return storage.get(fileName, FileInputStream.class);
    }

    public String savePic(InputStream pic) {
        var picName = storage.save(UUID.randomUUID().toString(), pic).orElseThrow(() -> new FileNotCreatedException(""));
        return URL_SERVER + picName;
    }





}
