package org.arcane.storage;

import org.arcane.exception.FileNotCreatedException;
import org.arcane.exception.MimeTypeNotDefined;
import org.arcane.exception.NotFoundFileException;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Optional;

public interface Storage {

    /**
     * Returns an instance of the class that was passed in the method parameters, however, the passed class must contain a constructor that accepts {@link String}
     *
     * @return  Returns a constructed instance of a class whose constructor accepts {@link String}
     * @throws  NotFoundFileException
     */
    <T extends InputStream> T get(String fileName, Class<T> streamType);
    InputStream get(String fileName);
    Optional<String> save(String fileName, InputStream file) throws MimeTypeNotDefined, FileNotCreatedException;
    void gc(LocalDateTime date);
    void delete(String fileName);
}
