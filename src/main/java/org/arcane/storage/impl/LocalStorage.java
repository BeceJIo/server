package org.arcane.storage.impl;

import io.quarkus.tika.TikaParser;
import io.sentry.Sentry;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.tika.mime.MimeTypeException;
import org.arcane.exception.FileNotCreatedException;
import org.arcane.exception.GenericNyazoException;
import org.arcane.exception.MimeTypeNotDefined;
import org.arcane.exception.NotFoundFileException;
import org.arcane.storage.Storage;
import org.jboss.logging.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LocalStorage implements Storage {

    private static final Logger LOG = Logger.getLogger(LocalStorage.class);

    private final Path rootDir;
    private final TikaParser parser;
    private final ExecutorService serviceExecutor = Executors.newSingleThreadExecutor();
    private final ConcurrentHashMap<String, String> fileNameCache = new ConcurrentHashMap<>();


    public LocalStorage(Path rootDir, TikaParser parser) {
        this.rootDir = rootDir;
        this.parser = parser;
        LOG.infov("Local storage is activated, work with files is defined in {0} directory", rootDir.toAbsolutePath().toString());
    }

    @Override
    public <T extends InputStream> T get(String fileName, Class<T> streamType)  {
        var filePath = Path.of(rootDir.toString(), fileName);
        if (!filePath.toFile().exists()) {
           throw new NotFoundFileException();
        }
        var accessTime = Instant.now();
        try {
            serviceExecutor.execute(() -> updateAccessTime(filePath, accessTime));
            return streamType.getConstructor(String.class).newInstance(filePath.toAbsolutePath().toString());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            Sentry.captureException(e);
            throw new GenericNyazoException("Internal error, plz contact admin");
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            Sentry.captureException(e);
            throw new NotFoundFileException();
        }
    }

    @Override
    public InputStream get(String fileName) {
        return this.get(fileName, FileInputStream.class);
    }

    @Override
    public Optional<String> save(String fileName, InputStream file) {
        Path filePath = Path.of(rootDir.toString(), fileName);
        try {
            filePath = createFile(file);
            try (var picWithOutExtension = new FileInputStream(filePath.toFile())) {
                var fileWithExtension = Path.of(rootDir.toString(), filePath.getFileName() + getFileExtension(picWithOutExtension));
                if (!filePath.toFile().renameTo(new File(fileWithExtension.toString()))) {
                    deleteFile(filePath);
                    throw new FileNotCreatedException("Failed to create file");
                }
                return Optional.of(fileWithExtension.getFileName().toString());
            }
        } catch (MimeTypeException e) {
            LOG.error("MimeType exception", e);
            deleteFile(filePath);
            throw new MimeTypeNotDefined();
        } catch (IOException e) {
            LOG.error("Storage exception", e);
            deleteFile(filePath);
            throw new FileNotCreatedException("Failed to create file");
        }
    }

    private String getFileExtension(InputStream pic) throws MimeTypeException {

        //var fileMetadata =  parser.getMetadata(pic);
        //return MimeTypes.getDefaultMimeTypes().forName(fileMetadata.getSingleValue("Content-Type")).getExtension();
        return ".png";
    }

    private String getFileExtension(Path pic) throws MimeTypeException, IOException {
        try (var fileStream = new FileInputStream(pic.toFile())){
            return getFileExtension(fileStream);
        }
    }

    private Path creteTempFile(InputStream file) throws IOException {
        var rootDirPath = rootDir.toString();
        var tempFile = Path.of(rootDirPath, UUID.randomUUID().toString());
        Files.copy(file, tempFile);
        return tempFile;
    }


    private Path createFile(InputStream file) throws IOException, MimeTypeException {
        Path filePath;
        String fileExtension;
        var rootDirPath = rootDir.toString();
        var tempFile = creteTempFile(file);
        fileExtension = getFileExtension(tempFile);
        do {
            filePath = Path.of(rootDirPath, RandomStringUtils.randomAlphanumeric(10) + fileExtension);
        } while (Files.exists(filePath) && !checkCacheFileName(filePath.toString()));
        if (!tempFile.toFile().renameTo(filePath.toFile())) {
            deleteFile(filePath);
            throw new FileNotCreatedException("Failed to create file");
        }
        deleteFileNameCache(filePath.toString());

        return filePath;
    }

    private boolean checkCacheFileName(String path) {
        synchronized (fileNameCache) {
            if (fileNameCache.containsKey(path))
                return true;
            fileNameCache.put(path, path);
            return false;
        }
    }

    private void deleteFileNameCache(String path) {
        fileNameCache.remove(path);
    }


    private void deleteFile(Path file) {
        try {
            Files.deleteIfExists(file);
        } catch (IOException e) {
            Sentry.captureException(e);
            LOG.error("unable to delete file", e);
        }
    }

    private Optional<Instant> fileAccessTime(Path filePath) {
        try {
            return Optional
                    .ofNullable(Files.readAttributes(filePath, BasicFileAttributes.class)
                    .lastAccessTime()
                    .toInstant());
        } catch (IOException e) {
            LOG.error("File Access Time exception", e);
            Sentry.captureException(e);
            return Optional.empty();
        }
    }

    private void updateAccessTime(Path filePath, Instant accessTime) {
        try {
            Files.getFileAttributeView(filePath, BasicFileAttributeView.class)
                    .setTimes(null, FileTime.from(accessTime), null);
        } catch (IOException e) {
            LOG.error("File access time setting failed", e);
            e.printStackTrace();
            Sentry.captureException(e);
        }
    }


    @Override
    public void gc(LocalDateTime date) {
        try (var files = Files.walk(rootDir)){
            files.filter(Files::isRegularFile)
                    .filter(file -> fileAccessTime(file)
                            .map(accessTime -> accessTime.isBefore(date.toInstant(OffsetDateTime.now().getOffset())))
                            .orElse(false))
                    .peek(this::deleteFile)
                    .forEach(x -> LOG.infov("File {0} deleted by garbage collector", x.getFileName()));
        } catch (IOException e) {
            LOG.error("Garbage collection exception", e);
            Sentry.captureException(e);
        }
    }

    @Override
    public void delete(String fileName) {
        deleteFile(Path.of(rootDir.toString(), fileName));
    }
}
