package org.arcane.auth;

public enum UserRole {
    GENERIC,
    ADMIN,
    REGULAR
}
