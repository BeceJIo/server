package org.arcane.auth.impl;

import io.quarkus.security.AuthenticationFailedException;
import org.arcane.auth.Auth;
import org.arcane.auth.User;
import org.arcane.auth.UserRole;
import org.jboss.logging.Logger;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Map;

public class BasicUserService implements Auth {

    private static final Logger LOG = Logger.getLogger(BasicUserService.class);
    private final Map<String, User> users;
    private static final User JOHN_DOE = new User("John Doe", "", UserRole.GENERIC);

    public BasicUserService(Map<String, User> users) {
        LOG.infov("Basic auth service is activated");
        this.users = users;
    }

    public User authenticate(String username, String password) {
        if (users.containsKey(username) && users.get(username).token().equals(password))
            return users.get(username);
        throw new AuthenticationFailedException();
    }

    @Override
    public User authenticate(String token) {
        byte[] decode = Base64.getDecoder().decode(token);
        String[] userData = new String(decode, Charset.defaultCharset()).split(":");
        return authenticate(userData[0], userData[1]);
    }


    @Override
    public UserRole getUserRole(String username) {
        return users.getOrDefault(username, JOHN_DOE).role();
    }
}
