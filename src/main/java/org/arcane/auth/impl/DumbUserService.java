package org.arcane.auth.impl;

import org.arcane.auth.Auth;
import org.arcane.auth.User;
import org.arcane.auth.UserRole;
import org.jboss.logging.Logger;

public class DumbUserService implements Auth {

    private static final Logger LOG = Logger.getLogger(DumbUserService.class);
    private static final User JOHN_DOE = new User("John Doe", "", UserRole.REGULAR);

    public DumbUserService() {
        LOG.infov("Dumb auth service is activated. Administrator functions are blocked, there is only one guest user {0} in the system with rights {1}", JOHN_DOE.username(), JOHN_DOE.role().toString());
    }

    @Override
    public User authenticate(String token) {
        return JOHN_DOE;
    }

    @Override
    public UserRole getUserRole(String token) {
        return JOHN_DOE.role();
    }

    @Override
    public User authenticate(String username, String password) {
        return JOHN_DOE;
    }

}
