package org.arcane.auth;

public record User(String username, String token, UserRole role) {
    public static final class UserBuilder {
        private String username;
        private String token;
        private UserRole role;

        private UserBuilder() {
        }

        public static UserBuilder anUser() {
            return new UserBuilder();
        }

        public UserBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public UserBuilder withToken(String token) {
            this.token = token;
            return this;
        }

        public UserBuilder withRole(UserRole role) {
            this.role = role;
            return this;
        }

        public User build() {
            return new User(username, token, role);
        }
    }
}
