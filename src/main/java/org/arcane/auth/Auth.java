package org.arcane.auth;

public interface Auth {

    UserRole getUserRole(String token);

    User authenticate(String username, String password);

    User authenticate(String token);
}
