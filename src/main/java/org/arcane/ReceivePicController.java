package org.arcane;

import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.http.ResponseEntity;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;

@Path("/upload.php")
public class ReceivePicController {

    private static final Logger LOG = Logger.getLogger(ReceivePicController.class);
    private final PictureService pictureService;


    public ReceivePicController(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed({"REGULAR", "ADMIN"})
    public ResponseEntity<String> receiveFile(@MultipartForm MultipartFormDataInput data) {
        try (InputStream pic = data.getFormDataMap().get("imagedata").get(0).getBody(InputStream.class, null)){
            return ResponseEntity.ok(pictureService.savePic(pic));
        } catch (IOException e) {
            LOG.error("An error occurred while getting from body multipart", e);
            return ResponseEntity.badRequest().build();
        }

    }

}
