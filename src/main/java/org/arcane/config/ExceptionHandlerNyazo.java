package org.arcane.config;

import org.arcane.exception.GenericNyazoException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandlerNyazo implements ExceptionMapper<GenericNyazoException> {

    @Override
    public Response toResponse(GenericNyazoException exception) {
        return Response.status(exception.getStatus().value()).build();
    }
}


