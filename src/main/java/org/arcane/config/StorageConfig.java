package org.arcane.config;

import io.quarkus.runtime.Startup;
import io.quarkus.tika.TikaParser;
import org.arcane.storage.Storage;
import org.arcane.storage.impl.LocalStorage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;
import java.time.Duration;
import java.time.temporal.TemporalAmount;
import java.util.Optional;

@Configuration
public class StorageConfig {

    private static final Logger LOG = Logger.getLogger(StorageConfig.class);

    @Autowired
    private TikaParser parser;

    @Value("${nyazo.server.storage.localstorage.rootpath}")
    private Optional<String> path;

    @Value("${nyazo.server.storage.type:LOCAL}")
    private Optional<String> type;

    private final String DEFAULT_WORKING_FOLDER = "pic";

    @Bean(name = "DurationGc")
    private TemporalAmount getDurationGc() {
        return Duration.ofDays(30);
    }

    @Bean
    @Startup
    public Storage getStorage() {
        switch (StorageType.valueOf(type.orElseThrow(() -> new RuntimeException("Storage type not specified")))) {
            case LOCAL -> {
                return new LocalStorage(Path.of(path.orElseGet(this::getDefaultDir)), parser);
            }
        }
        LOG.error("Storage not configured");
        System.exit(1);
        return null;
    }

    private String getDefaultDir() {
        var workingPath = Path.of("", DEFAULT_WORKING_FOLDER).toFile();
        if (!workingPath.exists() && !workingPath.mkdir())
            throw new RuntimeException("failed to create the default folder, please specify the folder manually or check the file system");
        return workingPath.getAbsolutePath();


    }
}
