package org.arcane.config;

import io.quarkus.runtime.Startup;
import org.arcane.PictureService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

@ApplicationScoped
@Startup
public class ScheduledJobs {

    private static final Logger LOG = Logger.getLogger(ScheduledJobs.class);

    private final PictureService pictureService;

    private final TemporalAmount durationGc;

    public ScheduledJobs(PictureService pictureService, @Qualifier("DurationGc") TemporalAmount durationGc) {
        this.pictureService = pictureService;
        this.durationGc = durationGc;
    }

    @Scheduled(cron= "{nyazo.server.shedule.files.gc}")
    public void scheduledCleaningStorage(){
        LOG.info("cleaning start");
        pictureService.deleteBeforeDate(LocalDateTime.now().minus(durationGc));
        LOG.info("cleaning completed successfull");
    }
}
