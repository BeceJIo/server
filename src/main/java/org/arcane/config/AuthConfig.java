package org.arcane.config;

import io.quarkus.runtime.Startup;
import org.arcane.auth.Auth;
import org.arcane.auth.User;
import org.arcane.auth.UserRole;
import org.arcane.auth.impl.BasicUserService;
import org.arcane.auth.impl.DumbUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Configuration
public class AuthConfig {

    @Value("${nyazo.server.admin.token}")
    private Optional<String> token;

    @Value("${nyazo.server.users.password}")
    private Optional<String> usersWithPassword;


    private final static String SPLIT_USER = ",";
    private final static String SPLIT_USER_DATA = ":";

    @Bean
    @Startup
    public Auth getAuthService(){
        var users = getUsers(usersWithPassword.orElse(""));
        return users.size() != 0 ? new BasicUserService(users) : new DumbUserService();
    }


    private Map<String, User> getUsers(String usersWithPassword) {
        Map<String, User> users = new HashMap<>();
        Arrays.stream(usersWithPassword.split(SPLIT_USER))
                .filter(x -> !x.isBlank())
                .map(x -> x.split(SPLIT_USER_DATA))
                .map(x -> User.UserBuilder.anUser()
                        .withUsername(x[0])
                        .withToken(x[1])
                        .withRole(UserRole.valueOf(x[2].toUpperCase(Locale.ROOT)))
                        .build())
                .forEach(x -> users.put(x.username(), x));
        return users;
    }



}
