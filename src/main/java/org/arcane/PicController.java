package org.arcane;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;

@RestController
public class PicController {

    private final PictureService pictureService;
    private final ClassLoader classLoader = getClass().getClassLoader();

    public PicController(PictureService pictureService1) {
        this.pictureService = pictureService1;
    }


    @GetMapping(value = "/{pic_name}", produces = "image/png")
    public Response getPic(@PathVariable(value="pic_name") String picName) throws IOException {

        if (picName.equals("favicon.ico")) {
            var favicon = Objects.requireNonNull(classLoader.getResource("favicon/favicon.ico")).openStream();
            return Response.ok(favicon).cacheControl(CacheControl.valueOf("public")).build();
        }
        return Response.ok(pictureService.getPic(picName)).cacheControl(CacheControl.valueOf("public")).build();
    }

    @GetMapping(value = "/all/delete")
    @RolesAllowed("ADMIN")
    public Response deleteBeforeDate() {
        pictureService.deleteBeforeDate(LocalDateTime.now());
        return Response.ok().build();
    }

    @GetMapping(value = "/delete/{pic_name}")
    @RolesAllowed("ADMIN")
    public Response deletePic(@PathVariable(value="pic_name") String picName) {
        pictureService.deletePic(picName);
        return Response.ok().build();
    }

}
